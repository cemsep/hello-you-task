﻿using System;
using System.Text.RegularExpressions;

namespace HelloYou
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;

            Console.WriteLine("Hey, what's your name?");

            while (!Regex.IsMatch(name = Console.ReadLine(), @"^[\p{L} \.\-]+$"))
            {
                Console.WriteLine($"Oups... Looks like {name} is not a valid name.");
            }

            Console.WriteLine($"Hello {name}, your name is {name.Replace(" ", "").Length} characters long and starts with a {name[0]}.");
        }
    }
}
