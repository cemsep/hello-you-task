A C# console app that:

- Display a console message which greets the user 
- Displays your name 
- Allow the user to input their name: example “XYZ” 
- Print “Hello XYZ, your name is 3 characters long and starts with a X.”